import { signal, effect, computed } from '@preact/signals'

const getTotalPrice = ({ quantity, price }) => quantity * price
const { format: formatCurrency } = new Intl.NumberFormat('fr-FR', {
  style: 'currency',
  currency: 'EUR',
  maximumFractionDigits: 0
})

document.addEventListener('DOMContentLoaded', () => {
  const bookElements = Array.from(document.querySelectorAll('.bon-de-commande tbody tr'))
  const books = signal(bookElements.map((el, index) => ({
    index,
    quantity: 0,
    price: parseFloat(el.dataset.price)
  })))

  const numberOfBooks = computed(() => books.value.reduce((total, { quantity }) => total + quantity, 0))
  const shippingTotal = computed(() => {
    return new Array(numberOfBooks.value)
      .fill(0)
      .map((value, index) => {
        switch (index) {
          case 0:  return 3;
          case 1:
          case 2:  return 2;
          default: return 1;
        }
      })
      .reduce((total, price) => total + price, 0)
  })

  const grandTotal = computed(() => books.value.reduce((total, book) => total + getTotalPrice(book), 0) + shippingTotal.value)

  effect(() => {
    document.querySelector('#shipping-value').textContent = formatCurrency(shippingTotal.value)
    document.querySelector('#total-value').textContent = formatCurrency(grandTotal.value)
  })

  bookElements.forEach(el => {
    const inputElement = el.querySelector('input[name^="book["')

    inputElement.addEventListener('input', (event) => {
      const index = parseInt(event.target.dataset.index, 10)
      const value = parseInt(event.target.value || '0', 10)

      if (isNaN(value)) {
        return
      }

      const totalPrice = getTotalPrice({ quantity: value, price: books.value.at(index).price })
      document.querySelector(`output#price-${index}`).textContent = formatCurrency(totalPrice)
      document.querySelector(`tr[data-book-index="${index}"]`).classList[value === 0 ? 'add' : 'remove']('sr-only')

      books.value = books.value.map(book => ({
        ...book,
        quantity: (book.index === index ? value : book.quantity)
      }))
    })
  })

  Array.from(document.querySelectorAll('.do-print')).forEach(el => {
    el.addEventListener('click', event => {
      event.preventDefault()
      window.print()
    })
  })
})
