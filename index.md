---
title: Des livres qui font du bien au cœur et à l'esprit…
htmlClass: accueil
# Le reste de cette page se trouve dans _includes/layout-accueil.njk
---
{%- extends 'layout-accueil.njk' -%}

{% block baseline %}
Des récits d'aventures alternatives et solidaires
{% endblock %}

{% block introduction %}
Les **Éditions REPAS** proposent des histoires bien concrètes et réelles d'économie sociale et d'autogestion, racontées par celles et ceux qui les vivent. Partez à la découverte de leurs aventures à travers notre collection originale de *Pratiques Utopiques*.

<span style="color: #ff9633">**NOUVEAU !**</span> Les éditions REPAS s'essaient au **podcast**. Des récits de pratiques autogestionnaires à écouter [sur notre Audioblog.](https://audioblog.arteradio.com/blog/215520/pratiques-utopiques)
Dernier épisode : Coopérer avec les Volonteux.
<iframe width="100%" src="https://audioblog.arteradio.com/embed/233375" style="margin: 0; padding: 0; border: none;"></iframe>

{% endblock %}

{% block chantiers %}
Les éditions REPAS proposent des **stages d'accompagnement à l'écriture collective** pour des structures engagées dans l'économie sociale et solidaire, qui souhaiteraient raconter leur parcours et étoffer notre collection de Pratiques Utopiques.
{% endblock %}



