---
title: Le réseau R.E.P.A.S.
description: Le Réseau d’Échanges de Pratiques Alternatives et Solidaires
header:
  image: Images/IMG_REPAS.jpg
htmlClass: taille-étroite
---

# Le réseau REPAS

[[sommaire]]

## R.E.P.A.S., c’est quoi ?
Le **R**éseau d’**E**changes et de **P**ratiques **A**lternatives et **S**olidaires
est composé d’entreprises coopératives, associations et lieux de vie engagés de façon permanente dans la concrétisation d’un projet de société. Il s’agit de favoriser une économie qui agit directement sur leur environnement de proximité, au service du territoire et des personnes.

Retrouvez l'actualité et les offres d'emploi du Réseau REPAS sur le site : [reseaurepas.free.fr](http://www.reseaurepas.free.fr/)


### Les structures de l’écosystème REPAS, c'est qui ?

Le réseau REPAS, depuis le début des années 1990, rassemble des dizaines de structures qui se reconnaissent de l'économie sociale et solidaire (ESS), de l'autogestion et de l'éducation populaire.
Agissant aussi bien dans l'agriculture, l’artisanat, les métiers de bouche, la scierie, la laine, les métiers du bâtiment, l’animation, le commerce, l’édition, les ressourceries, les arts et la culture, l’informatique... les structures du réseau REPAS sont aussi diversifiées dans leurs statuts que dans leurs activités : GAEC, Scop, SAPO, Association loi 1901, SCEA, GIE et bien d'autres, au gré de leurs besoins !

Les **collectifs en auto-gestion**, qu’ils soient des associations, des entreprises ou des lieux de vie, expérimentent une forme d’**innovation sociale et économique**. Leurs activités sont socialement utiles, respectueuses de l’environnement, ancrées sur le territoire et créatives dans leur approche de l’économie.
Ces collectifs revitalisent le développement en milieu rural, inventent de nouvelles relations entre producteurs et consommateurs, stimulent l’audace et l’initiative des jeunes. Ils répondent à la nécessité de repenser nos manières de consommer et nos façons de produire.

### Un peu d’histoire
En 1994, quelques entreprises et associations qui se connaissaient ou s’étaient rencontrées dans des réseaux alternatifs décident de se retrouver deux fois par an.
L’idée est de fonctionner en réseau informel, sans structure, sans permanent, avec une communication directe entre les acteurs : une rencontre au printemps, une autre à l’automne, avec un lieu et un thème de réflexion définis à la fin de la rencontre précédente.
Le réseau REPAS est né. Il fera rapidement naître le **compagnonnage REPAS** et les **éditions REPAS**.

## Qu’est ce que le compagnonnage REPAS ?

Les structures du réseau ont envie de s'entourer de nouvelles personnes, attirer, former, recruter dans de nouveaux horizons et essaimer leurs pratiques d'entrepreneuriat solidaire. L'idée d'un "compagnonnage" alternatif émerge en 1995. 2 ans plus tard, en 1997, la première promotion regroupe 8 Compagnon·ne·s, et 20 ans après, en 2017, ils sont 25.
Depuis 1997, 310 Compagnon·nn·es âgés de 18 à 40 ans se sont formés sur 23 Sessions de compagnonnages et ont été accueillis par plus d’une trentaine de structure.

Le parcours du compagnonnage permet d’interroger des manières de vivre et de travailler. Dans un parcours ponctué de temps de regroupements et d’immersions au sein des structures du réseau, c’est un cheminement dans l’expérimentation et la confrontation de ses propres représentations, principes et valeurs.

<a href="https://compagnonnage-repas.org/" class="cta"> Vers le compagnonnage R.E.P.A.S</a>




## Le R.E.P.A.S., mais pourquoi ?
#### «R» pour Réseau
Un réseau est une organisation qui existe réellement sans être pour autant structurée sous une forme fixe et définitive. Le réseau tisse des liens et permet les échanges de manière horizontale et évolutive. Il se modifie en fonction des projets et des rencontres.


#### «E» pour Echanges
Le E de REPAS témoigne d’une pratique fondatrice du réseau, celle de l’échange. Et ce n’est pas un hasard si le E a ce sens et non pas celui d’«Entreprises» ou d’«Économie», termes qui précèdent si souvent, presque inévitablement, les A et S d’alternative et solidaire... Pourquoi l’absence de ce mot que tant d’autres ont besoin d’afficher dans leurs initiales ? Parce que mettre en avant l’économie, c’est gommer bien des aspects de nos expériences qui, même économiques, dépassent largement ce cadre étroit, devenu hégémonique dans nos sociétés.

#### «P» pour Pratiques
C’est peut-être le mot le plus important. La légitimité des entreprises du réseau, de leur témoignage et de leur existence est dans la mise en route réelle et concrète de  pratiques alternatives et solidaires. Le réseau REPAS c’est d’abord un lieu où convergent des pratiques vécues au quotidien depuis de nombreuses années, un étonnant vivier d’initiatives, de réalisations et de constructions dont la seule existence est déjà une sacrée démonstration.

#### «A» et «S» pour Alternatives et Solidaires
Alternatives car nos pratiques chantent un petit air qui se démarque du grand orchestre de la mondialisation-marchandisation-économicisation du monde…Solidaires car elles privilégient l’association des individus, la mutualisation des projets et des biens et la coopération entre les êtres plutôt que l’individualisme, la privatisation et la compétition.

## Ils parlent de nous 
Le Réseau Repas fait la Une du numéro d'été 2023 de [Silence](https://www.revuesilence.net/), la revue de l'écologie politique et des alternatives. Un dossier complet nommé « REPAS, pratiquer l’utopie au travail », va à la rencontre de plusieurs structures du réseau (A Petits Pas, La Frênaie, Ambiance Bois, le compagnonnage…) et donne à voir la démarche des structures du réseau pour l’autogestion au travail. 
[Présentation et sommaire complet sur leur site](https://www.revuesilence.net/numeros/523-REPAS-pratiquer-l-utopie-au-travail/), où il est également possible de commander un numéro. 


![Couverture du numéro 523 de la revue Silence dédié au réseau REPAS](/Images/IMG_silence.jpg)