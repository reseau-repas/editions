---
title: Nos conditions de vente
header:
  image: Images/IMG_Depots.jpg
htmlClass: taille-étroite
---

# Nos conditions de vente

## Librairies

Nous accordons une remise de 35% sur le prix public des livres.
Nous expédions les commandes par la Poste et pratiquons les tarifications de port suivantes :
* 3,30€ pour 1 livre
* 4,90€ pour 2 livres
* 6.50€ pour 3 ou 4 livres

Attention, ces frais peuvent être modifiés en fonction du poids et du prix de l'ouvrage, ainsi que de l'évolution des tarifs postaux. A noter également que les livres les plus petits nécessitent des frais de port moins élevés.

Pour passer vos commandes via **DILICOM**, notre numéro **Gencod** : **3012485710012**.

Nos **[Conditions Générales de Ventes](https://editionsrepas.fr/mentions-legales/)** sont à retrouver sur la page Mentions Légales.

## Dépôts permanents

Les librairies et lieux accueillants du public peuvent faire le (bon) choix en devenant des lieux de dépôt permanents offrant toute l'année les livres des éditions REPAS à la vente.

Nos dépôts bénéficient de la même remise de 35% sur le prix public des livres, ainsi que d'**aucune charge de frais de port**. Pour en savoir plus ou devenir un lieu de dépôt, n'hésitez-pas à [nous contacter](mailto:repas@wanadoo.fr).

Retrouvez la **liste actualisée de nos lieux de vente permanente [sur cette page](/points-de-vente/).**

## Et à l'étranger ?

Les éditions REPAS acceptent bien volontiers d'expédier des ouvrages en dehors des frontières françaises, lorsque les coûts de distribution restent raisonnables. Nous envoyons ainsi nos livres à l'étranger proche (Suisse, Belgique) ainsi que dans l'Union Européenne et dans les territoires d'Outre-Mer français.
Pour toutes les autres destinations, merci de [nous contacter](mailto:repas@wanadoo.fr).
