---
title: Mention légales
htmlClass: taille-étroite
---
# Mentions légales

## Identité de l'association

L'association R.E.P.A.S. est une association loi 1901.
Elle est domiciliée au 4 allée Séverine, 26 000 Valence.

Numéro SIRET : 43486036700029
Numéro d'identification à la TVA : FR8043486036700029

Adresse mail de contact : repas@wanadoo.fr
Numéro de téléphone : 04.75.42.67.45


## Conditions Générales de Vente

#### Caractéristiques des biens
L'association R.E.P.A.S. commercialise des ouvrages produits par ses soins et imprimés en France. Elle commercialise également d'autres ouvrages de structures d'éditions avec lesquelles elle dispose d'accords de partenariat.

#### Fixation des prix
Le prix des ouvrages est imprimé sur chaque livre et disponible dans les pages du catalogue en ligne. L'association s'engage à fixer un prix de vente au public unique pour chaque ouvrage conformément à la [loi sur le prix unique du livre](https://www.sne.fr/prix-unique-du-livre/) du 10 août 1981.

#### Modalités de livraison
L'expédition des commandes est gérée directement par l'association R.E.P.A.S. dans un délai de 5 à 10 jours ouvrés. L'expédition se fait par la Poste. L'association REPAS décline toute responsabilité liée aux retards éventuels de livraison incombants aux services postaux.

#### Modalités de paiement
* Pour les commandes par correspondance, le client est tenu d'envoyer par voie postale un bon de commande imprimé avec la sélection d'articles de son choix, ses coordonnées permettant de procéder à l'expédition de la commande, ainsi qu'un chèque au montant exact, daté et signé à l'ordre de l'association REPAS.
* Pour les commandes en ligne, le client procède au réglement de sa commande par CB ou tout autre moyen de paiement mis à disposition par le site HelloAsso.
* Pour les commandes de professionnels, le paiement peut se faire par chèque (à l'ordre de l'association REPAS, à envoyer au : 4 allée Séverine, 26000 Valence) ou par virement bancaire.

#### Droit de rétractation
* Un client professionnel peut annuler une commande _avant_ l'expédition de la commande par la Poste ou procéder à un renvoi de la dite commande à ses frais. Ce renvoi fera alors l'objet d'un avoir, prenant en compte la valeur du bien renvoyé. Les frais de port ne sauront faire l'objet d'un avoir en cas de rétractation.
* Un client particulier peut user de son droit de rétractation dans les 14 jours suivant sa commande auprès des éditions REPAS. Il devra pour cela faire parvenir un écrit exprimant sa volonté de rétracter sa commande. Le simple renvoi du bien sans déclaration ou le refus de prendre livraison ne suffisent pas à exprimer sa volonté de se rétracter. Suite à une rétractation, les éditions REPAS procèdent au remboursement intégral du client incluant les frais de port initiaux du bien, mais n'incluent pas les frais de renvoi le cas échéant.

#### Politique de confidentialité

L’association REPAS s’engage à respecter et à appliquer la réglementation en vigueur en France (Loi Informatique et Libertés du 6 janvier 1978 modifiée) et dans l’Union Européenne (Règlement UE 2016/679 du Parlement européen et du Conseil du 27 avril 2016 relatif à la protection des personnes physiques à l’égard du traitement des données à caractère personnel et à la libre circulation de ces données , dit « RGPD ») en matière de protection des données à caractère personnel. Chaque formulaire limite la collecte des données personnelles au strict nécessaire afin de respecter la minimisation des données.

Les informations recueillies sur ce site ont pour unique usage de faire parvenir les commandes à l'adresse de livraison indiquée par le commanditaire. Aucune commercialisation n'est faite, ni aujourd'hui ni dans l'avenir, de ces informations. Seule l'adresse de courriel, si elle est indiquée, sera ajoutée à nos fichiers informatisés dans un objectif de communication autour de nos prochaines parutions. Toutes les autres données personnelles données via ce formulaires ne sont pas conservées au-delà de 6 mois.
Vous pouvez accéder aux données vous concernant, les rectifier, demander leur effacement ou exercer votre droit à la limitation du traitement de vos données. Consultez le site cnil.fr pour plus d’informations sur vos droits.


Pour toute information ou exercice de vos droits Informatique et Libertés sur les traitements de données personnelles gérés par les éditions REPAS, vous pouvez contacter l'association par mail ou par courrier.

---

#### Coordonnée de l'hébergeur du site :  ####
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**Cliss XXI**
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;23 avenue Jean Jaurès, 62800 Liévin
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;contact@cliss21.com
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;03 21 45 78 24

---
