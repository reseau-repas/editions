---
title: Récits de chantiers d'écriture
header:
    image: Images/IMG_Chantier2.jpg
htmlClass: taille-étroite
---

# Chantiers d'écriture en cours

## Ressourcerie Court-Circuit

La Creuse est recouverte d'une neige épaisse lorsque nous rencontrons le collectif de la [**ressourcerie Court-Circuit**](https://www.facebook.com/RessourcerieCourtsCircuits/) à Felletin, en Janvier 2023. Une douzaine de personnes nous attend pour retracer cette histoire qui débute en 2010. Une histoire collective autour de la revalorisation des déchets, comme un prétexte pour revaloriser le territoire, les personnes qui s'y impliquent et les parcours personnels chaotiques qui s'y rencontrent. Il s'agit d'abord de prendre soin et d'accueillir tous les publics, à rebour de la polarisation et des clivages qui séparent trop souvent les habitants d'un territoire.
Le temps d'un week-end, nous offrons l'espace de retracer ce chemin, d'apprendre les uns sur les autres et de comprendre tout ce qu'il y a de commun dans les étapes de cette histoire.

Le collectif de Court-Circuit, friand de se raconter, a décidé de mener cette écriture à plusieurs, à cinq, six ou plus suivant les envies de chacun. Un nouveau défi pour eux comme pour nous !


## Ferme éducative de la Batailleuse

A la mi-novembre 2022, nous voilà partis en Franche-Comté à la rencontre de la [**ferme de la Batailleuse**](https://claj-batailleuse.fr/) et de son centre d'accueil collectif de mineurs.
Pendant trois jours, avec près de quinze personnes, nous retraçons les **40 ans d'histoire** de cette structure originale, multi-façettes et très engagée. Souvenirs souvenirs,depuis les premiers pas dans les années 1980 jusqu'à nos jours et l'ébullition de projets, le fournil, les soirées ouvertes au village, la production de comté bio, et la rénovation du centre d'accueil pour investir sur l'avenir...

C'est Milena et Matthieu qui ont désormais la tâche exigeante de transformer tous ces témoignages en un récit fidèle, qui nous plonge au coeur de cette **aventure agri-pédagogique**. Les éditions REPAS seront bien sûr à leurs côtés pour les aider dans cette phase de rédaction prévue toute au long de l'année 2023 !


## Hameau du Viel Audon

Notre premier chantier d'écriture s'est déroulé dans le hameau du **Viel Audon**, un village en ruines, rebâti depuis plus de 50 ans par des chantiers de jeunes. Il abrite aujourd'hui une petite ferme et un centre de formation pédagogique.
Ce lieu unique et hors du temps en Ardèche a déjà raconté son histoire, dans l'ouvrage *[Chantier Ouvert au Public](/catalogue/chantier-ouvert/)* publié aux éditions REPAS en 2009. Toute son histoire ? Non, les quinzes dernières années ont été denses et aussi riches d'apprentissages que les précédentes, c'est pourquoi nous lançons une **réédition augmentée** de ce livre. L'autrice, Béatrice Barras, change de statut et devient accompagnatrice de deux nouvelles plumes, Claire et Maria.

Lors d'un week-end au Viel Audon, en Janvier 2022, nous avons travaillé le déroulé des événements de ces quinze dernières années, les thématiques transversales et les styles d'écriture pour ce texte à quatre mains. Les autrices se sont revues à de nombreuses reprises tout au long de l'année pour continuer la rédaction. La publication de la réédition augmentée de *Chantier ouvert au public* est prévue pour le printemps 2023.

![4 personnes souriant pendant un chantier d'écriture au Viel Audon](/Images/IMG_0154.JPG)
