---
title: Appel à contributions - Chantiers d'écriture
htmlClass: taille-étroite
---

# Appel à contributions

## Des chantiers d'écriture pour transmettre des pratiques utopiques concrètes

L’association REPAS (Réseau d’échanges et de pratiques alternatives et solidaires) organise des rencontres et publie des récits d’initiatives alternatives. Notre parti-pris : des histoires racontées par ceux qui les vivent.
Une vingtaine d’ouvrages sont parus dans ce cadre : https://editionsrepas.fr

**Ces récits d’alternatives concrètes** sont précieux et utiles, d’une part parce qu’ils permettent aux acteurs de **prendre du recul sur leur expérience** pour mieux la faire avancer, et d’autre part parce qu’ils sont **inspirants pour d’autres**, pour passer à l’action.
Le passage à l’écriture n’est pas toujours facile, par manque de temps, parce qu’on ne se sent pas légitimes pour porter le témoignage d’une expérience collective, ou que l’on redoute de faire ressortir des conflits... Pour ces raisons et sans doute encore quelques autres, **beaucoup d’histoires ne sont pas racontées** alors qu’elles sont souvent des germes de réponses pertinentes à des enjeux de société très importants.


*Nous avons désespérément besoin d’autres histoires (...) des histoires techniques à propos de ce type de réussite, des pièges auxquels il s’est agi, pour chacune, d’échapper, des contraintes dont elle a reconnu l’importance, bref des histoires qui portent sur le penser ensemble comme oeuvre à faire.* - Isabelle Stengers, philosophe

Notre collection « Pratiques Utopiques » est riche de témoignages assez longs et détaillés, qui ont pris parfois plusieurs années à être rédigés. Aujourd’hui il nous parait **urgent de faire savoir et transmettre le foisonnement d’expériences nouvelles qui sont à l’œuvre par des textes plus courts**, peut-être moins exhaustifs, mais centrés sur un ou plusieurs enjeux majeurs qui sont représentatifs de l’expérience.
Pour **faciliter la collecte de ces récits**, nous voulons expérimenter des chantiers d’écriture de 2 à 4 jours, dans lesquels nous accompagnons les acteurs et actrices à la rédaction de leur histoire.

### Méthode proposée
Le groupe arrive avec ses documents existants : objets, photos,
dessins, écrits, dépliants, rapports d’AG, coupures de presse...
et démarre le chantier réparti sur 4 journées.

#### Jour 1 | Avec tout le collectif
* Petite et grande histoire du collectif
* Identifier ses enjeux transversaux, ses périodes structurantes
* Construire un récit partagé|

#### Jour 2 | Avec les auteurs & autrices volontaires
* Déterminer le fil rouge du récit
* Construire le synopsis et le plan détaillé
* Ateliers individuels d’écriture

#### Jour 3 | Avec les auteurs & autrices volontaires
* Ateliers individuels d’écriture
* Relectures et échanges collectifs
* Apports méthodologiques selon les besoins (menée d’entretiens, hiérarchisation de l’information, écrire pour le grand public...)

#### Jour 4 | Avec les auteurs & autrices volontaires
* Ateliers individuels d’écriture
* Mise en commun des productions écrites
* Les étapes suivantes pour faire aboutir le livre
* Conclusion - bilan

Ces 4 journées peuvent être à la file, ou coupées en deux fois 2 journées. Elles peuvent être suivies d’un accompagnement à la finalisation de la rédaction.
À terme, il s’agit d’offrir un outil clé en main de réalisation de témoignages, devant aboutir sur un livre édité par les éditions REPAS dans les mois qui suivent le séminaire.

Les éditions REPAS sont à la **recherche d’équipes qu’un tel projet pourrait motiver pour l’expérimenter** et affiner notre démarche d’intervention. À ce titre, nous ne demandons aucune rémunération mais seulement une solution d’hébergement gracieux le temps du chantier d’écriture.

*Ce qui se passe sur le terrain en ce moment semble un levier prioritaire pour aller vers un scénario positif supposant une évolution profonde de la culture économique, sociale et politique. L’avenir n’est pas écrit : donner plus de puissance à ces « petits récits » est indispensable pour fabriquer un « grand récit » associationniste, audible plus largement et susceptible de convaincre.* - Christine Chognot, sociologue

## Avec qui ?
**Béatrice Barras** a participé à trois expériences alternatives dont elle a témoigné à travers la rédaction de trois ouvrages. Engagée dans le comité éditorial des éditions REPAS depuis sa fondation, elle a accompagné plusieurs projets. Dans le cadre de l’association ACTE1, elle participe à une recherche action sur l’autobiographie raisonnée (inspirée d’H. Desroche) appliquée aux organisations.

**Valentine Porche** est salariée-coordinatrice des éditions REPAS et animatrice de réseaux associatifs. Elle anime de nombreux ateliers, réunions, séminaires à l’aide d’outils d’éducation populaire et encadre des formations avec l’association d’éducation nouvelle CEMEA.

**Michel Lulek** a participé à plusieurs expériences autogérées dont il a témoigné à travers la rédaction de plusieurs ouvrages. Professionnellement, il est depuis 15 ans rédacteur dans la Scop La Navette et écrit articles, guides, portraits pour des associations ou des entreprises de l’ESS. Il a également accompagné des auteurs dans la rédaction de leur ouvrage.

## Contact

Valentine Porche, Béatrice Barras et Michel Lulek
Association REPAS
4 allée Séverine, 26 000 Valence
repas@wanadoo.fr
04 75 42 67 45
