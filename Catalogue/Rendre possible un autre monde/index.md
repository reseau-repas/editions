---
Collection: Presses de l'Economie Sociale
DépotLégal: 2009
Réédition: 5ème ed. revue et augmentée 2024
Auteur: Jean-François Draperi
Titre: Rendre possible un autre monde
SousTitre: Economie sociale, coopératives et développement durable
ISBN : 978-2-952385-40-8
Pages: 80 pages
Prix: 7
Etat: Disponible
Résumé: |
    "Un autre monde est possible" affirment les mouvements alter-mondialistes. Mais quel autre monde ? Et comment le bâtir ? C'est la question qu'aborde ce petit livre à l'attention de celles et ceux qui veulent changer leur vie et contribuer à changer le monde. Ce manuel pour une utopie praticable revisite les fondements des coopératives, mutuelles et associations de différents pays pour mettre en valeur leur contribution au développement territorial durable mais aussi un autre rapport au travail.

    Rédacteur en chef de la Revue internationale de l'économie sociale (Recma), **Jean-François Draperi** est maître de conférence en sociologie au Conservatoire national des arts et métiers où il dirige le Centre d'Economie Sociale Travail et Société (CESTES/Cnam).
Tags:
- Coopérative
- Economie sociale et solidaire
- Développement rural
- Alternatives
- Collectif
Couverture:
Couleurs:
  Fond: '#bc718f'
  Titre: '#ffffff'
  Texte: '#ffffff'
Fonts:
  Titre: mermaid
  SousTitre: inherit
---
