---
Collection: Presses de l'Economie Sociale et Fédération Nationale des Coopératives de Consommateurs
DépotLégal: 2017
Auteur: Jean-François Draperi
Titre: Coopérer pour consommer autrement
ISBN : 978-2-918522-04-1
Pages: 85 pages
Prix: 8
Etat: Disponible
Résumé: |
    Notre consommation doit s'adapter, voire changer en profondeur, afin de mieux préserver la santé et l'environnement. Elle ne le fera que si son coût est supportable pour la majorité de la population. L'enjeu central de l'essor d'une production et d'une consommation soutenables peut ainsi résumé en cette équation : la qualité des produits à un prix raisonnable.

    Depuis plus de cent cinquante ans, des hommes et des femmes ont tenté de répondre à ce défi. Leur histoire se déploie en cinq temps : avant 1912, la création des coopératives fondatrices; dans les années 20, la mise en œuvre d'une organisation qui projette de fonder une République coopérative; après la Seconde Guerre mondiale, l'essor d'un mouvement d'ampleur internationale au service de l'intérêt général; dans les années 80, la crise de ce mouvement; et actuellement, une renaissance des coopératives de consommateurs. Ce livre présente l'histoire féconde de cette aventure.

    **Jean-François Draperi** est directeur du Centre d'économie sociale (Cestes) au Conservatoire national des arts et métiers (le Cnam) et rédacteur en chef de la Revue internationale de l'économie sociale (Recma).
Tags:
- Coopération
- Collectif
- Economie sociale et solidaire
- Alternatives
CommandeWeb: https://www.helloasso.com/associations/association-repas/paiements/cooperer-pour-consommer-autrement-jean-francois-draperi?_ga=2.216268694.1091905683.1671446978-84470218.1590573003
Couverture:
Couleurs:
  Texte: '#ffffff'
  Titre: '#ffffff'
  PageTitres: '#eb5a3b'
AccueilOrdre: 
Fonts:
  Titre: cocogoose
  SousTitre: inherit
  Collection: inherit
  Auteurice: inherit
---
