---
Edition: Editions REPAS
DépotLégal: 2023
Auteurs: Nicolas Posta et Béatrice Maurines
Titre: Aux champs les Volonteux
SousTitre: Une ferme collective, un tiers-lieu nourricier
Collection: Collection Pratiques Utopiques
ISBN : 978-2-919272-21-1
Pages: 184 pages
Prix: 18 
Etat: Disponible
Résumé: |
    Une ferme collective où travaillent près de 30 personnes : maraîchers, épicières, arboriculteurs, boulangers, herboriste, fripière... Un tiers-lieu ouvert sur son territoire, la Drôme, où l'on vient faire son marché et dont le jardin pédagogique est investi par les écoles environnantes. Une ferme familiale depuis quatre générations qui s'est transformée en une Scop-coopérative d'activités et d'emploi, structure originale qui permet à chacun de développer des activités variées au cœur d'un projet commun. Une ferme qui tisse des liens avec de nombreux partenaires des mondes agricole, militant ou culturel. Tout cela, c'est la « Ferme des Volonteux », un nom hérité du lieu qui évoque bien ce que sont les femmes et les hommes qui font vivre aujourd'hui cette belle histoire.
    Au-delà de ce qui s'y fabrique, s'y vend et s'y produit, on y cultive aussi la coopération, l'entraide, la mutualisation et le commun. Décrite et analysée avec détail par Nicolas Posta et Béatrice Maurines, la ferme est également racontée à travers les paroles des Volonteux. Le lecteur y découvrira une agriculture réinventée, ancrée et innovante.

    _« Si je n’avais qu’une seule chose à retenir, c’est le bonheur que me procure cette ferme. Cette petite étincelle quand tous les travailleurs se mettent en ordre de bataille pour un objectif commun, un évènement, une vision. Ceci me donne un vrai espoir de voir le monde changer car l’Homme, comme la nature, peut opérer des changements très rapides. Il faut qu’il en ait la volonté. »_
    Rémy Léger, Volonteux.

    [**Le site internet de la ferme des Volonteux.**](https://www.fermedesvolonteux.com/ )
Tags:
- Coopérative
- Développement rural
- Agriculture bio
- Economie sociale et solidaire
- Tiers-lieu
SiteWeb: https://www.fermedesvolonteux.com/ 
CommandeWeb: https://www.helloasso.com/associations/association-repas/paiements/aux-champs-les-volonteux?_ga=2.127476877.1269261566.1692794138-1726830621.1692794138&_gl=1%2a1115m0r%2a_ga%2aMTcyNjgzMDYyMS4xNjkyNzk0MTM4%2a_ga_TKC826G3G2%2aMTY5Mjc5NDE1OC4xLjEuMTY5Mjc5NDY2NS41MS4wLjA.
TrouverLibrairie: https://www.placedeslibraires.fr/listeliv.php?base=allbooks&mots_recherche=aux+champs+les+volonteux
Bandeau: Nouveauté
AccueilOrdre: 1
Adresse: Le Volonteux, 26760 Beaumont-lès-Valence
Latitude: 44.85505567179505
Longitude: 4.950264552314299

Couleurs:
  Titre: '#EDDB9B'
  Fond: '#88AD35'
  Texte: '#fdf1cc'
  PageTitres: '#678529'
---
## A Ecouter

Pour poursuivre ou initier la lecture, écoutez les Volonteux et Volonteuses raconter leur vision de la coopération dans notre podcast. Une immersion sonore dans le quotidien de leur ferme, et une mise à jour des nouveaux projets qui germent dans cette terre fertile aux bonnes idées !
<iframe width="100%" src="https://audioblog.arteradio.com/embed/233375" style="margin: 0; padding: 0; border: none;"></iframe>

Retrouvez les autres épisodes du podcast "Pratiques Utopiques" sur notre [Audioblog Arte Radio.](https://audioblog.arteradio.com/blog/215520/podcast/233375/chez-les-volonteux-un-modele-paysan-cooperatif)


## Extraits

> Nous pouvons croire à la politique des petits pas, des colibris, et de la COP 27, mais la vérité, tout du moins la mienne, c’est que tout le monde s’accroche au « non-changement » pour garder ses petits privilèges. Sans même se rendre compte que demain, nous ne pourrons peut-être plus subvenir à nos besoins élémentaires en eau, en nourriture… Le changement ne pourra être que brutal, d’une façon ou d’une autre.
Mais malgré ce dur constat, la Ferme est mon îlot, mon ADN, ma façon de vivre ce monde, et en attendant que l’humanité veuille bien changer, nous préparons, peut-être, le levain de demain. 
> -- Rémi, Volonteux. page 162

> Pour moi, nous n’inventons rien, nous ne faisons que réinventer. Nous cuisinons une bonne tambouille avec mille et une idées déjà passées, pensées, travaillées et digérées par tant de générations avant nous.
Lorsqu’on arrive à une bonne recette, c’est ce qui nous démarque des autres et c’est aussi ce qui peut marquer les esprits, voire même qui sait, l’histoire.
> -- David, Volonteux. page 160

> Le collectif des Volonteux est évidemment militant, mais ne se situe pas dans un militantisme « post-it »1 - soit un engagement ponctuel autour de causes multiples mené sur une courte durée. Les membres de la Ferme sont très actifs pour leur collectif qui se construit pas à pas, bien qu’à distance relative de l’engagement politique ou politicien. Aux Volonteux, on n’aspire pas à l’avènement du « Grand Soir ».
À la Ferme, l’engagement repose d’abord sur une démarche indi¬viduelle ancrée sur le temps long : chacun participe à la transfor¬mation du monde par ses actes, et ce, de façon pragmatique. Mais aux Volonteux, l’engagement est aussi collectif et on s’appuie sur ces deux formes de militantisme pour participer au changement du monde dans lequel on vit.
> -- p58-59


