---
Edition: Editions REPAS
DépotLégal: 2011
Auteurs: Association Grand’Air & P’tits Bonheurs
Titre: Soignants Chanteurs
SousTitre: Un monde à plusieurs voix
Préface: Préface de Jean-Pierre Olives
Collection: Collection Pratiques Utopiques
ISBN : 978-2-919272-02-0
Pages: 137 pages
Prix: 16
Etat: Indisponible
Résumé: |
    A la fin des années 1990, à Toulouse, suite à la rencontre entre un chanteur lyrique, Bertrand Maon, et des membres du personnel de l'hôpital public, se crée un groupe de « soignants chanteurs » : ce ne sont pas des chanteurs qui soignent, mais des soignants qui chantent...

    Les soignants chanteurs de [l'association Grand air & p’tits bonheurs](https://www.enfancemusique.asso.fr/liens/grand-air-ptits-bonheurs/) inscrivent leur action dans une vision de l'hôpital à l'opposé de ce qu'il a tendance à devenir. Maintenant, en 2011, l’hôpital est devenu une entreprise, et le malade, un client. Dans cet univers administratif comptable, impitoyable, l’individu peut perdre son âme, et le malade la compassion, le respect et la dignité. Heureusement, il existe encore, comme le chantait Georges Brassens « des assoiffés d’Azur, des poètes, des fous ».

    C’est une expérience professionnelle et humaine qu’ils veulent transmettre aujourd’hui par ce livre-célébration : communiquer leur enthousiasme, susciter et encourager les projets artistiques et, ainsi, oeuvrer à ce que professionnels et institutions osent la transgression, se nourrissent de son énergie et ouvrent des interstices sociaux de liberté et de plaisir.
Tags:
- Collectif
- Alternatives
- Soin
SiteWeb: https://www.enfancemusique.asso.fr/liens/grand-air-ptits-bonheurs/
CommandeWeb: https://www.ardelaine.fr/librairie-pratiques-alternatives-solidaires/62-livre-soignants-chanteurs-editions-repas-alternatives-et-solidaires.html
TrouverLibrairie: https://www.placedeslibraires.fr/livre/9782919272020-soignants-chanteurs-un-monde-a-plusieurs-voix-association-grand-air-p-tits-bonheurs/
Couverture:
Couleurs:
  Fond: '#c3d2e0'
  Texte: '#285d8c'
  Titre: '#285d8c'
  PageTitres: '#275a89'
Bandeau: Epuisé
Adresse: 39 All. du Var, 31770 Colomiers
Latitude: 43.595455
Longitude: 1.33044
---

## Les auteurs

« L'hospitalisation peut être un moment de souffrance et de rupture. La musique, le chant permettent de proposer sans les imposer des moments de rencontre entre tous les acteurs de l'hôpital. Cela exige pour nous une capacité d'accordage avec l'enfant, sa famille, et les professionnels qui l'entourent. »

Camille et Florence sont éducatrices de jeunes enfants, Marjolaine, Caroline, Anne Marie, Nadine et Christine sont puéricultrices, Monique, Martine, Jully sont auxiliaires de puériculture, Jackie est puéricultrice cadre de santé, Corinne, pédiatre, Yannis, puériculteur, Bertrand, chanteur lyrique, Dédé et Pierrot, techniciens et retraités. Ce sont les acteurs de l'histoire de Grand air & P'tits bonheurs et les auteurs de ce livre collectif.

## Extraits

> Il ne passait pas inaperçu ! Et pourtant il y a eu du temps avant que je ne le croise. Je cherchais un professeur de chant. Je rencontrai un magicien de la voix. La voix… la sienne bien sûr, chaleureuse et enveloppant l'espace, les pommettes et le regard rieurs, même quand il chante. Avec toujours cette attention à la garder bien placée, quand il parle surtout. Mais magicien par le don qu'il a de libérer en chacun de nous notre propre voix, avec simplicité, avec proximité et respect, avec patience, avec rigueur, exigence, avec une attention toujours renouvelée, dans cet équilibre fragile qui touche le plus profond de l'être, le chant.
> -- page 62

> L'atelier terminé, nous nous retrouvons à la salle Philippe Noiret, pour écrire ces moments vécus à chaud. C'est un sas de partage des émotions, un moment de confrontation de nos ressentis, le lieu finalement de la distanciation.
> L'écriture est facilitée par quelques notes prises au cours de l'après-midi, juste le prénom et l'âge  des enfants rencontrés : cela nous permet, en échangeant, de tirer le fil de chaque histoire. Notre « cahier classeur » est toujours à disposition, à l'accueil, et sert, en plus, de lien avec les personnels mais aussi d'archive, de mémoire.
> Il nous permet de prendre conscience de notre évolution, de notre continuité ; d'affiner nos pratiques au fil des années, de les remettre en question. Nous sommes toujours étonnés de la vivacité des souvenirs à la lecture d'ateliers, mêmes anciens.
> -- page 75
