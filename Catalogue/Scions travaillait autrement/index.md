---
Edition: Editions REPAS
DépotLégal: 2003
Réédition: 2009
Auteur: Michel Lulek
Titre: Scions… travaillait autrement ?
SousTitre: Ambiance Bois, l'aventure d'un collectif autogéré
Préface: Préface de Serge Latouche
Collection: Collection Pratiques Utopiques
ISBN : 978-2-9520180-7-4
Pages: 174 pages
Prix: 15
Etat: Indisponible
Résumé: |
    À 20 ans, au lieu de changer le monde, ils décident de changer leur vie et de créer ensemble une entreprise pour y expérimenter d'autres formes d'organisation du travail. Ce sera une scierie, Ambiance Bois, qui s'installera en 1988 sur le plateau de Millevaches dans le Limousin.

    De fil en aiguille, ce ne sont pas seulement les modalités classiques de production qui seront remises en cause, mais la place que cette dernière occupe dans nos vies. Ainsi, les associés d'Ambiance Bois découvriront que « travailler autrement », c'est consommer, agir, décider et finalement « vivre ensemble ».

    [**Ambiance Bois aujourd'hui**](https://www.ambiance-bois.com/)
Tags:
- Coopérative
- Collectif
- Développement rural
- Alternative
- Travail
- Economie sociale et solidaire
SiteWeb: https://www.ambiance-bois.com/
CommandeWeb: https://www.helloasso.com/associations/association-repas/paiements/scions-travaillait-autrement-michel-lulek?_ga=2.177291173.1091905683.1671446978-84470218.1590573003
TrouverLibrairie: https://www.placedeslibraires.fr/livre/9782952018074-scions-travaillait-autrement-ambiance-bois-l-aventure-d-un-collectif-autogere-michel-lulek/
Couverture:
Couleurs:
 Fond: '#5F9B63'
 PageTitres: '#397049'
 Titre: ' #FFFFCD'
 Texte: '#FEFBDE'
Adresse: Fermerie, 23340 Faux-la-Montagne
Latitude: 45.722253
Longitude: 1.913715

---

## L'auteur

**Michel Lulek** est un des six cofondateurs d'Ambiance Bois. Il a participé avec Marc Bourgeois, également d'Ambiance Bois, à la rédaction de l'ouvrage « Quand l'entreprise apprend à vivre » consacré à l'expérience du compagnonnage alternatif et solidaire du réseau REPAS (éditions Charles Léopold Mayer, 2002). Il est également l'auteur dans la collection « Pratiques utopiques » du livre *Télé Millevaches, la télévision qui se mêle de ceux qui la regardent*.

## Extraits

> Communauté : le mot au demeurant ne nous convenait guère. Il recouvrait certes notre réalité, mais sa connotation ou religieuse ou trop soixante-huitarde nous semblait fausser l’expérience que nous vivions. Nous faisions notre vie un point c’est tout et ne voulions surtout pas nous encombrer des clichés que véhicule l’expression. Ni hippies, ni iréniques troubadours du retour à la nature ; encore moins trappistes ou esprits échauffés de quelque assemblée charismatique, nous savions que les mots sont parfois des prisons lorsqu’ils vous identifient à l’image figée d’un qualificatif. Aussi, lorsque au bout de quelques mois nous avons créé une association pour nous représenter collectivement, n’est-ce surtout pas ce mot-là que nous avons mis en exergue. Nous lui avons préféré celui de « collectif ».
> -- page 90

> Premièrement, tout le monde prend sa part des tâches de production répétitives, en général plutôt fatigantes physiquement et guère intéressantes en soi.
> Deuxièmement, chacun acquiert plusieurs compétences différentes, qu’il ne sera pas forcément seul à maîtriser, mais qu’il dominera suffisamment pour pouvoir assumer un poste assez spécialisé.
> Troisièmement, les spécialités ne sont pas attribuées *ad vitam aeternam*. On apprend de nouvelles choses, on cumule des savoirs et même si on ne les utilise plus un temps, on peut réintervenir ponctuellement sur tel ou tel secteur qu’on a déjà pratiqué. De la même façon il n’y a pas de chasses gardées et chacun peut, s’il le souhaite, apprendre un nouveau « métier », même très éloigné de ses compétences de départ.
> Quatrièmement, chacun doit avoir un pied dans les ateliers et un pied dans les bureaux. L’atelier est le lieu où s’exécutent des commandes et le bureau celui où elles se prennent.
> -- page 113

## Du même auteur

* *Télé Millevaches, La télévision qui se mêle de ceux qui la regardent*, Samuel Deléron, Michel Lulek, Guy Pineau, 2006, Editions REPAS. [Voir la fiche du livre](/catalogue/tele-millevaches/)
* *Pour que l'argent relie les Hommes. 40 années de réflexion et d'expérimentation*, Association La NEF, avec la collaboration de Michel Lulek, 2018, Editions REPAS. [Voir la fiche du livre](/catalogue/pour-que-largent-relie-les-hommes/)
