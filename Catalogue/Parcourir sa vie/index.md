---
Collection: Presses de l'Economie Sociale
DépotLégal: 2010
Réédition: 2016
Auteur: Jean-François Draperi
Titre: Parcourir sa vie
SousTitre: Se former à l'autobiographie raisonnée
ISBN : 978-2-918522-03-4
Pages: 224 pages
Prix: 10
Etat: Indisponible
TrouverLibrairie: https://www.placedeslibraires.fr/livre/9782918522034-parcourir-sa-vie-se-former-a-l-autobiographie-raisonnee-nouvelle-preface-draperi/
Résumé: |
    L'autobiographie raisonnée est un exercice personnel qui permet de mieux se connaître et d'éclairer les choix d'orientation, par exemple pour reprendre une formation, fonder un projet professionnel ou se lancer dans une nouvelle activité sociale.

    De mise en œuvre aisée, elle consiste à réfléchir sur sa propre histoire de vie, affirmant que pour savoir où l'on va, il faut se souvenir d'où l'on vient. Elle est à la fois orale et écrite : orale, parce que la prise de parole est essentielle au souvenir ; écrite afin de fixer la mémoire de façon raisonnée et durable.
    L'autobiographie raisonnée est également la première étape d'un projet de recherche-action, c'est-à-dire d'une pratique de changement de ses propres activités sociales et professionnelles. Elle est un outil privilégié de l'implication et du changement social, particulièrement adaptée à l'animation associative et mutualiste et au management coopératif dans le cadre d'un projet d'économie sociale et solidaire. Elle forge le lien social et l'amitié, premières conditions de l'action collective.

    Ce livre est construit en deux parties, l'une pratique et l'autre théorique, qui offrent au lecteur la possibilité de pratiquer l'entretien et de réfléchir sur ses ressorts et ses effets d'un triple point de vue éducatif, sociologique et philosophique.

    Rédacteur en chef de la Revue internationale de l'économie sociale (Recma), **Jean-François Draperi** est maître de conférence en sociologie au Conservatoire national des arts et métiers où il dirige le Centre d'Economie Sociale Travail et Société (CESTES/Cnam).
Tags:
- Analyse de pratique
- Economie sociale et solidaire
CommandeWeb: https://www.helloasso.com/associations/association-repas/paiements/parcourir-sa-vie-jean-francois-draperi?_ga=2.211341461.1091905683.1671446978-84470218.1590573003
Couverture:
Bandeau:  Nouvelle préface et 2ème édition
AccueilOrdre: 8
Fonts:
  Titre: mermaid
  SousTitre: inherit
Couleurs:
  Fond: '#4a658c'
  Texte: '#fff'
  Titre: '#fff'
  PageTitres: '#4a658c'
---
