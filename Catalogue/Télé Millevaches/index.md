---
Edition: Editions REPAS
DépotLégal: 2006
Auteurs: Samuel Deléron, Michel Lulek, Guy Pineau
Titre: Télé Millevaches
SousTitre: La télévision qui se mêle de ceux qui la regardent
Préface: Poème-préface de Raoul Sangla
Collection: Collection Pratiques Utopiques
ISBN : 978-2-9520180-3-6
Pages: 144 pages
Prix: 15
Etat: Disponible
Résumé: |
    Ce livre raconte l'histoire de [Télé Millevaches](https://telemillevaches.net/), une télévision locale qui, parmi les premières en France, et aujourd'hui l'une des plus anciennes encore en activité, témoigne de l'appropriation par des habitants de l'outil télévisuel pour communiquer, échanger, montrer ce qui se fait sur leur territoire et porter une parole que les télévisions ignorent en général.

    Télévision de proximité, de pays, associative, de quelque façon qu'on l'appelle, Télé Millevaches se raconte ici à plusieurs voix. Le récit de cette aventure, qui constitue la première partie de l'ouvrage, a été écrit par un des membres de l'équipe fondatrice et est complété par des entretiens avec des acteurs de cette histoire. La seconde partie resitue l'histoire de Télé Millevaches dans celle, plus large, des télévisions de proximité en France et des évolutions télévisuelles de 1986 à 2006.
Tags:
- Alternative
- Médias
- Développement rural
SiteWeb: https://telemillevaches.net/
CommandeWeb: https://www.helloasso.com/associations/association-repas/paiements/tele-millevaches-samuel-deleron-michel-lulek-guy-pineau?_ga=2.244461573.1091905683.1671446978-84470218.1590573003
TrouverLibrairie: https://www.placedeslibraires.fr/livre/9782952018036-tele-millevaches-la-television-qui-se-mele-de-ceux-qui-la-regardent-michel-lulek-samuel-deleron-guy-pineau/
Couverture:
Couleurs:
 Fond: '#a8bdbe'
 PageTitres: '#c94369'
Adresse: 23340, Faux-la-Montagne
Latitude: 45.749
Longitude: 1.938
---

### Les auteurs

**Michel Lulek**, cofondateur de Télé Millevaches, est également l'auteur dans la même collection « Pratiques utopiques » du livre « Scions… travaillait autrement. Ambiance Bois, l'aventure d'un collectif autogéré » (2003).
**Samuel Deléron** est journaliste et travaille à Télé Millevaches.
**Guy Pineau** est sociologue, ancien responsable de recherche à l'INA (Institut national de l'audiovisuel) et chargé d'enseignement à Paris III. Il est membre actif de la Fédération Nationale des Vidéos de Pays et des Quartiers.

## Extrait

> L'aventure de Télé Millevaches était lancée. Les numéros s'enchaînèrent aux autres, proposant des sommaires qui sortaient parfois des seules communes où ils étaient diffusés, pour montrer une initiative intéressante ou illustrer une problématique locale qui se déclinait ailleurs de la même manière.
> En se nommant Télé Millevaches, la petite télé de Faux-la-Montagne et des quelques communes environnantes s'était dès le début identifiée au territoire plus vaste sur lequel elle était située. Qu'on soit en Creuse du côté de Royère ou Flayat, en Corrèze du côté de Meymac ou Bugeat, en Haute-Vienne à Nedde ou Rempnat, la réalité est la même. Les problèmes démographiques ou économiques, les enjeux de territoire, les évolutions agricoles et forestières, les perspectives touristiques, sociales ou culturelles sont similaires.
> -- page 113

## Le commentaire des éditeurs

Les médias forment-ils l'opinion ? La télévision reflète-t-elle le monde et la société ? N'est-elle forcément qu'une affaire de professionnels ? A ce genre de questions qui reviennent régulièrement dans l'actualité, l'expérience de Télé Millevaches n'apporte certes pas de réponses catégoriques mais ouvre des perspectives tout à fait originales.

Au cours des vingt années dont l'histoire est rapportée ici avec beaucoup de détails, le lecteur pourra en effet découvrir comment une poignée d'habitants du plateau de Millevaches a réussi à dompter un outil en le mettant au service d'un territoire et de ceux qui y vivent. S'inscrivant en cela dans une véritable démarche de développement local, cette expérience dessine en creux ce que n'est pas, ne sait pas être ou ne veut pas être la télévision (la vraie, la grande, celle qui vous dépossède de votre parole, même quand elle fait semblant de vous la donner).

Comme l'écrit Daniel Salles dans Alternatives Economiques : « Une télé partie de rien, qui a su trouver sagement son public, son matériel et les moyens de sa pérennité financière. Une "télé brouette" pour commencer, qui venait installer son téléviseur et son magnétoscope dans les cafés, les maisons de retraite ou chez des particuliers qui rameutaient le voisinage. Puis le journal vidéo des débuts s'est mué en "magazine du plateau" pour 120 communes. Ensuite, une télévision qui faillit être diffusée la nuit sur le canal régional de France 3 lorsqu'il dort et automatiquement enregistrée grâce à un système ingénieux. Et il faut lire à ce propos la manière dont ces Géo Trouvetou de la télé ont été baladés, en 1993, par le CSA, TDF, le ministère de la Communication et France 3, ligués pour faire capoter ce projet hors norme ! » Qu'importe ! 22 ans après sa naissance Télé Millevaches fait toujours entendre sa petite musique différente sur le plateau de Millevaches, et désormais, grâce à Internet, bien au-delà.

Ce livre nous raconte cette saga que les "vraies" télés sont incapables de faire. Il raconte les interrogations, les rapports avec les élus locaux, l'organisation, etc. Un exercice pratique et obstiné de développement local et de lien social.

## Du même auteur...

* *Scions… travaillait autrement ? Ambiance Bois, l'aventure d'un collectif autogéré*, Michel Lulek, 2009, Editions REPAS. [Voir la fiche du livre](/catalogue/scions-travaillait-autrement/)
