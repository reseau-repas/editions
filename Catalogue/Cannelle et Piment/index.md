---
Edition: Editions REPAS
DépotLégal: 2021
Autrice: Agnès Rollet
Titre: Cannelle et Piment
SousTitre: L’histoire d’une entreprise associative au féminin
Préface: Préface de Jean-Louis Laville
Collection: Collection Pratiques Utopiques
ISBN: 978-2-919272-16-7
Pages: 204 pages
Prix: 17
Etat: Disponible
Résumé: |
    Elles s’appellent Fatna, Micheline, Juliet, Abla, Eugénie, Haïfa, Chimène… Elles viennent des 4 coins du monde (Algérie, la Réunion, Irak, Togo, Tunisie, Chili…) et se sont croisées à Vaulx-en-Velin, banlieue emblématique de la région lyonnaise, mais surtout terre d’accueil de la diversité. Leur recette :  le besoin de mettre du beurre dans les épinards de leur famille, le désir de faire connaître leurs cultures pour lutter contre les préjugés et de transmettre des héritages culinaires, et une dynamique de groupe portée par les méthodes d’émancipation de Paulo Freire.

    Sans diplôme, elles ont développé une activité de traiteur multiculturel, sous la forme d’une entreprise associative, et jouent un rôle d’exemplarité au pied des tours de leur quartier auquel elles sont restées fidèles. Cannelle et piment, c’est une aventure d’économie solidaire qui ne rentre dans aucune case institutionnelle, et qui, après avoir eu bien du mal à se faire reconnaître, n'a plus besoin aujourd'hui, du haut de ses 30 ans, de faire ses preuves.
    
    [**Site internet de l'association.**](http://www.cannelle-et-piment.fr/)

    
Tags:
- Femmes
- Autogestion
- Collectif
- Quartiers populaires
- Economie sociale et solidaire
SiteWeb: http://www.cannelle-et-piment.fr/
CommandeWeb: https://www.helloasso.com/associations/association-repas/paiements/cannelle-et-piment-agnes-rollet?_ga=2.243914757.1091905683.1671446978-84470218.1590573003
TrouverLibrairie: https://www.placedeslibraires.fr/livre/9782919272167-cannelle-et-piment-l-histoire-d-une-entreprise-associative-au-feminin-agnes-rollet/?utm_source=dmg
AccueilOrdre: 4
Adresse: 15 Rue Auguste Renoir, 69120 Vaulx-en-Velin
Latitude: 45.778037
Longitude: 4.928796
Vidéos:
  https://rcf.fr/culture-et-societe/linvite-de-lete-rcf-lyon?episode=135295: « Cannelle et piment, une entreprise au féminin » sur RCF Lyon
Couleurs:
  Titre: '#f6e8bb'
  PageTitres: '#e3893a'
  Fond: '#D67822'
  Texte: '#f6e8bb'
---

## L'autrice

**Agnès Rollet**, avec son bagage en socio-ethnologie, a débuté auprès des enfants de la rue au Brésil. Critique des rapports de domination Nord-Sud, elle a ensuite préféré s'impliquer localement, dans sa région lyonnaise, pour plus de justice sociale et dans le monde associatif. Son moteur dans l'action : les initiatives qui valorisent les "sans voix". C'est ce qu'elle tente de faire en tant que directrice d'un centre social depuis 15 ans.

## Extraits

> Elles enfournent, comptent, transvasent, découpent les aliments. Les plats et les boîtes défilent, les mains se croisent et les frigos se remplissent. L’une d’elles, Anissa, une jeune mère de famille nouvellement arrivée, vérifie pour la troisième fois les bons de commande afin de s’assurer que rien ne sera oublié. C’est bientôt l’heure de la pause repas. Les deux buffets prévus pour midi sont déjà partis avec la camionnette de livraison à 11 heures avec Abdel et Malik, les deux livreurs du moment
> -- page 20

> *Bonjour, je travaille au centre social et je cherche des habitants qui ont envie de faire des choses pour le quartier.*
>
> Fatna qui n’aime pas ouvrir la porte à des inconnus ne peut se retenir face à cette voix féminine, chaleureuse et dynamique, avec un accent peu commun à Vaulx-en-Velin. Curieuse, elle entrouvre la porte.
Chimène explique que depuis quelques temps, des femmes du quartier, de toutes origines, se retrouvent deux fois par semaine, les lundis et les vendredis, dans un local au milieu des immeubles. Les femmes parlent ensemble de leur quotidien, de leur pays d’origine, de la vie dans le quartier, des difficultés avec la langue française, des enfants, de leur éducation et de l’école, mais aussi des fins de mois difficiles. Cela fait du bien de se retrouver, surtout celles qui n’ont pas de famille à proximité. Elles se rendent compte qu’elles ne sont pas seules et que beaucoup d’entre elles vivent les mêmes choses. Alors elles comprennent que la situation des banlieues, ce n’est pas de leur faute, c’est un problème de société plus large. Ensemble, elles se donnent des idées, des astuces pour mieux s’en sortir, elles échangent leurs expériences, mais surtout, entre femmes, elles se donnent du courage.
> -- pages 69-70

## Le commentaire de Jean-Louis Laville

> Cette aventure menée par des femmes à Vaulx-en-Velin, différentes mais réunies autour de leur projet commun, ne s’est pas déroulée sans encombre. Si ce livre est passionnant, c’est qu’il explicite à la fois les aléas rencontrés et les conquêtes obtenues. Dans la période de mutations actuelle, avec l’aggravation des inégalités sociales et de dérèglements écologiques, il n’est plus possible de s’en remettre à une technocratie qui croit surplomber et commander la société. Des expériences comme celle de Cannelle et Piment doivent aider à réfléchir pour qu’un nouveau cadre institutionnel favorise les citoyennes qui s’engagent pour un mieux-vivre.
> -- Jean-Louis Laville, chercheur en économie sociale au CNAM
