---
Edition: Editions REPAS
DépotLégal: 2018
Auteurs: Textes et témoignages collectifs
Préface: Photographies de Sarah Ney
Titre: La Cantine des Pyrénées en lutte
Collection: Collection Pratiques Utopiques
ISBN : 978-2-919272-12-9
Pages: 96 pages
Prix: 12
Etat: Disponible
Résumé: |
    Que faire de l’idée révolutionnaire quand la situation ne l’est pas ? La question est grande et offre de multiples possibilités de réponses qui, jamais figées, demandent surtout à être expérimentées pratiquement. L’histoire de La Cantine des Pyrénées fait partie de ces nombreuses tentatives qui s’efforcent, en renouant avec des formes de solidarité en résistance au capitalisme, d’instituer autre chose dans les pratiques et les relations sociales.

    Ce lieu autogéré, d'abord installé rue des Pyrénées, dans le XXe arrondissement de Paris, se présente dans cet ouvrage en photos (celles de Sarah Ney) et en textes. Plus d'une vingtaine de témoins, de toutes origines, racontent leur rencontre avec La Cantine et ce qu'ils y ont trouvé. Car au-delà des actions qui y sont menées (une cantine, des permanences pour les sans-papiers ou les mal-logés, des cours de français, du cinéma, etc.), ce sont des femmes et des hommes qui font de ce lieu tout autre chose qu'un restaurant ou un centre social. Un endroit où se croisent aussi des luttes, autour d'un repas de soutien ou d'un débat, que ce soit celles de Notre-Dame-des-Landes, des ouvriers de Peugeot en grève ou des migrants.

    La Cantine des Pyrénées ne se veut pas LA réponse qu’il faudrait reproduire. Elle n’est qu’une tentative pratique parmi d’autres. Mais, en période d’apathie politique comme en période de mouvement social, les espaces collectifs d’auto-organisation comme elle, sont appelés à se multiplier. Travaillant avec ce qui est, La Cantine s’efforce ainsi de montrer, par la pratique, que le monde ne va pas de soi et que, par conséquent, il est possible d’y agir autrement.

    L'actu de la Cantine c'est [**par ici.**](https://www.facebook.com/cantinedespyrenees/?locale=fr_FR) 
Tags:
- Collectif
- Autogestion
- Quartiers populaires
SiteWeb: https://paris-luttes.info/+-la-cantine-des-pyrenees-+
CommandeWeb: https://www.helloasso.com/associations/association-repas/paiements/la-cantine-des-pyrenees-en-lutte?_ga=2.185127849.1091905683.1671446978-84470218.1590573003
TrouverLibrairie: https://www.placedeslibraires.fr/livre/9782919272129-la-cantine-des-pyrenees-en-lutte-collectif-sarah-ney/
Couverture:
Couleurs:
  Fond: '#4a4a4a' #ou test #e5544a
  Titre: '#ffffff'
  Texte: '#ffffff'
  PageTitres: '#e5544a'
Fonts:
  Titre: linux-libertine
  SousTitre: inherit
Adresse: 77 Rue de la Mare, 75020 Paris
Latitude: 48.873304
Longitude: 2.388705
---

## Extraits

> Depuis l’extérieur, tout portait à croire que La Cantine était une brasserie classique. L’ancienne pancarte « Au bar des amis » était d’ailleurs restée encastrée au mur et surplombait la façade rouge. Lorsqu’on poussait la porte, le mobilier intérieur faisait également penser à un café. Un comptoir d’environ deux mètres partait de l’entrée et longeait le mur droit pour s’arrêter au niveau d’un petit espace légèrement surélevé. Ce dernier fonctionnait à la manière d’un passe-plat lors des repas du midi, puisqu’il faisait la jonction entre la cuisine, située derrière lui, et l’espace de restauration, le plus grand, à gauche de l’entrée, et faisant face au comptoir.
> Si nous avons fait le choix de ne pas charger la décoration de marqueurs explicitement politiques ou esthétiques, c’était, d’une part, pour ne pas restreindre symboliquement l’accès aux seuls activistes et initiés des squats, d’autre part, pour limiter notre participation, sous couvert de « lieu alternatif », au processus de gentrification déjà bien engagé dans le XXe arrondissement.
>
> Cette esthétique épurée participait de notre volonté d’ouverture à un public qui ne partageait pas nécessairement notre culture politique. De la même façon, nous avons décidé de ne pas ouvrir La Cantine le soir. Cependant, La Cantine n’était pas un lieu comme les autres. Plusieurs affiches permettaient à ceux qui poussaient la porte de se situer. Près de l’entrée, une première affiche expliquait brièvement pourquoi le prix du repas étaient si bas (pas de loyer, pas de salariés, pas de bénéfices) et commençait par « Ici ce n’est pas un restaurant ».
> -- page 14

> La Cantine des Pyrénées ? Un îlot rare à Paris où une « senior » comme moi reprend espoir qu’un « autre monde » est encore possible, où on oublie, pour un moment, le pouvoir de l’argent, les clivages sociaux et culturels, l’agressivité et l’indifférence. Que ce soit à midi autour d’un repas de qualité (toujours des menus qui nous surprennent par leur inventivité) ou le soir aux cours d’alphabétisation où se côtoient en ce moment des personnes provenant d’au moins quatorze pays différents ! Je suis toujours frappée par l’ambiance chaleureuse qui y règne, de convivialité, de partage, de disponibilité et d’écoute des uns envers les autres.
> -- Citation de Dolores, page 74
