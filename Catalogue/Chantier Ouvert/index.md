---
Edition: Editions REPAS
DépotLégal: 2008
Autrice: Béatrice Barras
Titre: Chantier ouvert au public
SousTitre: Le Viel Audon, village coopératif
Collection: Collection Pratiques Utopiques
ISBN : 978-2-9520180-6-7
Pages: 192 pages
Prix: 17
Etat: Disponible
Résumé: |
    Lorsqu’au début des années 1970 quatre copains découvrent les ruines abandonnées du hameau ardéchois du Viel Audon et décident de lui redonner vie, ils ne savent pas ce qu’ils déclenchent. C’est le début d’une aventure qui accueillera sur ce « chantier ouvert au public » plus de 10 000 jeunes qui apporteront leur pierre à l’édifice.

    Mais le [Viel Audon](https://levielaudon.org/) n’est pas seulement un lieu où l’on construit. C’est aussi un lieu où l’on se construit. Le chantier devient école et les jeunes qui y passent, expérimentent un « chemin de faire » pour mener leur propre route. Ce hameau de Balazuc, blotti dans les gorges de l’Ardèche, toujours inaccessible en voiture, recèle un « trésor » qu’il partage avec tous ses visiteurs.
Tags:
- Coopération
- Jeunesse
- Education populaire
SiteWeb: https://levielaudon.org/
CommandeWeb: https://www.helloasso.com/associations/association-repas/paiements/chantier-ouvert-au-public-beatrice-barras?_ga=2.214503575.1091905683.1671446978-84470218.1590573003
TrouverLibrairie: https://www.placedeslibraires.fr/livre/9782952018067-chantier-ouvert-au-public-le-viel-audon-village-cooperatif-3e-edition-beatrice-barras/
Couleurs:
  Fond: '#EA444F'
  Titre: '#feefcf'
  Texte: '#feefcf'
  PageTitres: '#e72d39'
Adresse: 07120 Le Viel Audon, Balazuc
Latitude: 44.50581
Longitude: 4.362983
Vidéos:
  https://france3-regions.francetvinfo.fr/auvergne-rhone-alpes/ardeche/ardeche-a-la-decouvert-du-viel-audon-un-ecovillage-en-partage-dans-chroniques-d-en-haut-sur-france-3-2081650.html: "Le Viel Audon : Un village en partage dans « Chroniques d’en haut » - France 3 AuRA – 9 mai 2021"
---

## L'autrice

**Béatrice Barras** a fait partie de la petite équipe qui a entreprit dans les années 1970 la restauration du village du Viel Audon. C'est aussi une des fondatrices de la Scop Ardelaine dans laquelle elle travaille toujours. Elle a également assumé diverses responsabilités auprès de différentes structures de l'économie sociale et solidaire, en particulier lorsqu'elle fut présidente du comité d'éthique de la NEF.

## Extrait

> Nous revenons le lendemain et nous sommes sidérés devant le travail réalisé. Les broussailles ont disparu et le tas de gravats a bien diminué. Gérard, piqué au jeu, se met à l'œuvre avec eux. Les pioches attaquent, les pelles volent, les seaux évacuent les gravats. Chacun se coordonne aux autres pour être plus efficace : il y a ceux qui piochent, ceux qui pellent et remplissent les seaux et ceux qui les vident plus loin... et la chaîne tourne. Après deux jours, nous voyons apparaître l'aménagement de la source réalisé par les habitants d'autrefois et l'on distingue bien la sortie de l'eau. Les jeunes sont fiers et enthousiasmés par ce qu'ils ont réalisé. On les voit grandis.
> -- page 41

## L'histoire

Lorsqu'au début des années 1970 quatre copains découvrent les ruines abandonnées du village ardéchois du Viel Audon et décident de lui redonner vie, ils ne savent pas ce qu'ils déclenchent. C'est le début d'une aventure qui verra passer sur ce « chantier ouvert au public » plus de 10 000 personnes qui apporteront chacune une pierre à l'édifice. Mais le Viel Audon n'est pas seulement un lieu où l'on construit.

C'est aussi un lieu où l'on se construit. Le chantier devient école et les jeunes qui passent y expérimentent un « chemin de faire » pour mener leur propre route. Le hameau blotti dans les gorges de l'Ardèche, toujours inaccessible en voiture, bruissant d'une vie riche et innovante, recèle un « trésor » qu'il partage avec tous ses visiteurs.

Ce livre raconte sur plus de trente cinq ans comment des ruines oubliées sont devenues un lieu de vie, d'apprentissage, de formation, de découverte, un véritable village coopératif et écologique.

## Le commentaire des éditeurs

Dans la France bouillonnante des années qui suivent 1968, s'expérimentent dans les montagnes ou les campagnes reculées du sud de la France une foultitude d'essais communautaires, arches de résistance, groupuscules politiques, « retour à la nature »... S'il est de bon ton aujourd'hui de regarder ces expériences d'un air au mieux goguenard, on ne peut nier que s'échafaudaient-là des réponses parfois très pertinentes aux questions que notre société du début du XXIème siècle se pose. C'est le cas dans ce hameau accessible seulement à pied qui sommeillait dans les ronces et les herbes folles, accroché à la falaise dans les gorges de l'Ardèche.

L'aventure commence comme un rêve. Quatre jeunes décident de faire revivre ce village. Ils relèvent leurs manches et s'attèlent à la tâche sans plan préconçu ni un sou en poche. Trente cinq plus tard, le randonneur qui traverse par hasard le Viel Audon (les voitures n'y ont toujours pas accès !) découvrira un ensemble architectural impressionnant, des jardins, des bêtes, une exploitation agricole qui fabrique du fromage de chèvres et croisera de très nombreuses personnes dont beaucoup de jeunes. Entre ces deux extrèmes (le village abandonné d'hier et la ruche d'aujourd'hui) que s'est-il passé ?

C'est cette aventure que raconte Béatrice Barras qui a pour l'occasion interrogé de nombreux acteurs de cette histoire dont la parole nous est ainsi restituée. L'accent est mis sur la dimension pédagogique et coopérative de ce projet, vaste chantier de jeunes qui a marqué la plupart de ceux qui y sont passés.

Comme dit l'un d'eux : « Sur le chantier j'ai appris à prendre des responsabilités et j'ai vu que j'étais capable de faire des choses dans lesquelles je ne connaissais rien avant. Chez moi, j'avais essayé de construire un petit bâti dans le jardin de mon père et j'avais pris deux baffes parce que ce n'était pas comme ça qu'il fallait faire ! Au Viel Audon, j'ai découvert la possibilité de me dire que j'étais capable. Il y avait les filles et à cette époque, la mixité ce n'était pas encore acquis ! Et puis, il y a la notion de collectif : on découvre qu'on a une capacité à jouer collectivement et que c'est un facteur incroyable de réussite : on change de registre, on ne cherche plus à se dire « je suis le plus beau, le plus fort », on passe du « je » au « nous », mais un nous qui est plus que la somme des « je » ; le « nous » devient un esprit différent. En fait, le chantier, c'est une formidable école de la vie ! »


## Du même auteur...

* *Moutons Rebelles. Ardelaine, la fibre développement local*, Béatrice Barras, 2014, Editions REPAS. [Voir la fiche du livre](/catalogue/moutons-rebelles/)
* *Une Cité aux mains fertiles*, *Quand les habitants transforment leur quartier*, Béatrice Barras, 2019, Editions REPAS. [Voir la fiche du livre](/catalogue/une-cite-aux-mains-fertiles/)

