---
Collection: Presses de l'Economie Sociale
DépotLégal: 2009, 7ème ed.
Auteur: Jean-François Draperi
Titre: L'économie sociale
SousTitre: Utopies, Pratiques, Principes
ISBN : 978-2-952385-49-1
Pages: 128 pages
Prix: 5
Etat: Indisponible
Résumé: |
    L'économie sociale est à la croisée des chemins. Elle poursuit son projet de définition d'une économie originale à travers une histoire presque bicentenaire. Au cours de cette histoire, elle a successivement rêvé d'une communauté alternative, puis d'une république coopérative et enfin d'un développement intégré à l'échelle des territoires. Régulièrement mise en danger par l'économie capitaliste, par l'Etat mais aussi par ses propres insuffisances, elle a dû s'organiser, édifier des principes de référence, susciter l'élaboration d'un droit définissant ses règles de conduite.

    Face à une idéologie ultra-libérale envahissante, l'économie sociale propose un ensemble coordonné de valeurs et d'actions qui permet de penser différemment le rapport entre l'économie et la personne humaine.

    Saura-t-elle trouver les forces de résister à la marchandisation croissante de la vie ? Ce livre revient sur cette histoire en présentant une démarche originale : l'économie sociale résulte de la coordination entre un mouvement de pensée - changer la vie économique et sociale par des moyens non-violents - et un mouvement de pratiques - créer des entreprises au service des hommes.

    S'adressant aux militants de l'économie sociale et aux étudiants en sciences économiques et sociales, il approfondit la perspective ouverte dans le premier livre de cette collection paru sous le titre *[Rendre possible un autre monde](/catalogue/rendre-possible-un-autre-monde/)*.
Tags:
- Economie sociale et solidaire
- Coopératives
CommandeWeb: https://www.helloasso.com/associations/association-repas/paiements/l-economie-sociale-jean-francois-draperi?_ga=2.209785749.1091905683.1671446978-84470218.1590573003
Couverture:
Couleurs:
  Fond: '#F8B63A'
  Titre: '#3e3e3e'
  Texte: '#4a4a4a'
---
